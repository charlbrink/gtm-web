'use strict';

var app = angular.module('gtmApp', [
    'ngRoute',
    'ngResource',
	'gtmControllers'
]);

app.constant('serviceUrls', {
    codelistUrl: 'http://localhost:8080'
});

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
    /*
    .when('/home', {
        templateUrl: 'templates/index.html',
        controller: 'HomeController'
    })
    */
	.when('/declaration/:declarationIdentification', {
		templateUrl: 'partials/declaration.html',
		controller: 'DeclarationDetailsCtrl'
	 })
	.otherwise({
		redirectTo: '/declaration/1'
	});
}]);

app.factory("Codelist",['$resource', 'serviceUrls', function ($resource, serviceUrls) {

    return $resource(serviceUrls.codelistUrl + "/v1/codelist/:codelist/", {codelist: "@codelist"}, {
    });
}]);

app.factory("CodelistEntry", function ($resource) {
    return $resource("/v1/codelist/:codelist/:code/", {codelist: "@codelist", code: "@code"}, {
    });
});
