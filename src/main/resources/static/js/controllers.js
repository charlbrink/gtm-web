var codelistControllers = angular.module('gtmControllers', []);

const CURRENCY_CODELIST = "currency";
const CURRENCY_ZAR = "ZAR";

codelistControllers.controller('DeclarationDetailsCtrl',[ '$scope', '$http', 'Codelist' , 
		function($scope, $http, Codelist) {
			$http.get('declarations/declaration.json').success(function(data) {
				$scope.declaration = data;
				$scope.currencyCodelist = Codelist.query({codelist: "currency"});
				$scope.declarationEditDisabled = true;
			});
		}
]);

codelistControllers.controller('CurrencyCodelistEntry',
		function($scope, $http, $locale) {
			$http.get(getCodelistURL(CURRENCY_CODELIST, CURRENCY_ZAR)).success(function(data) {
				$scope.entry = data;
			});
		});

codelistControllers.controller('CurrencyCodelist', function($scope, $http, $locale) {
	$http.get(getCodelistURL(CURRENCY_CODELIST)).success(
			function(data) {
				$scope.codelist = data;
				$scope.amountCurrency = $scope.codelist[getEntryIndex($scope.codelist, getEntryByCode($scope.codelist, "usd"))];
			});
});
